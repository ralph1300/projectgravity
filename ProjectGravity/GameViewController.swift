//
//  GameViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 21.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit

class GameViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var LogoLabel: UILabel!
    @IBOutlet var GameView: UIView!
    @IBOutlet weak var GameTableView: UITableView!
    @IBOutlet weak var NameFilterTextField: UITextField!
    @IBOutlet weak var SegmentControl: UISegmentedControl!
    var socket:SocketIOClient = SocketIOClient(socketURL: "");
    var gameList:Array<Filter> = [];
    var gameNames:Array<String> = [];
    var selectedRowForSegue:Int = 0;
    let cellIdentifier = "GameNameCell";
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        GameTableView.dataSource = self;
        GameTableView.delegate = self;
        
        setCustomFondsAndColors();
        self.addHandlers();
        let f = Filter();
        self.askServerForGameList(f);
    }
    
    func setCustomFondsAndColors(){
        LogoLabel.font = UIFont (name: "DK Crayon Crumble", size: 60);
        LogoLabel.textColor = UIColor(red:0.235 , green: 0.58, blue: 0.55, alpha: 1.0);
        GameView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        let nib = UINib(nibName: "cstmGameTvCell", bundle: nil);
        self.GameTableView.registerNib(nib, forCellReuseIdentifier: cellIdentifier);
        
    }
    
    func addHandlers(){
        //Adds Handlers for the Socket, so this View can work with data from the server
        //Expected Format:
        //"{;games;:[{;id;:0,;name;:;Ping Pong;,;player;:2},{;id;:1,;name;:;Tetris;,;player;:3},{;id;:2,;name;:;Space Invaders;,;player;:1}]}"
        
        self.socket.on("loadGameList") {[weak self] data, ack in
            
            var jsonString:String = (data?[0] as! String).stringByReplacingOccurrencesOfString(";", withString: "\"");
            jsonString.removeRange(Range<String.Index>(start: jsonString.startIndex, end:jsonString.startIndex.advancedBy(9)));
            jsonString = String(jsonString.characters.dropLast());
            var dictTest = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!;
            var error:NSError?;
            let anyObj: AnyObject?
            do {
                anyObj = try NSJSONSerialization.JSONObjectWithData(dictTest, options: NSJSONReadingOptions(rawValue: 0))
            } catch var error1 as NSError {
                error = error1
                anyObj = nil
            } catch {
                fatalError()
            };
            var list = HelperMethods.parseJsonToFilterArray(anyObj!);
            
            self!.addNewGamesToTable(list);
            
        }
    }
    
    func askServerForGameList(f:Filter){
        //Sends a request for a GameList, is also triggered when to Filter has changed
        let jsonString = HelperMethods.JSONStringify(f.filterToDictionary()).stringByReplacingOccurrencesOfString("\"", withString: ";");
        
        self.socket.emit(Statics.LOAD_GAMES,jsonString);
    }

    @IBAction func PlayerCountSelectionChanged(sender: AnyObject) {
        filterChanged();
        
    }
    @IBAction func FilterChanged(sender: AnyObject) {
        filterChanged();
    }
    func filterChanged(){
        var newFilter:Filter = Filter();
        var playerCount = SegmentControl.selectedSegmentIndex;
        if(playerCount == 0){
            newFilter = Filter(_id: 0, _name: NameFilterTextField.text, _player: 1);
        }else if(playerCount == 1){
            newFilter = Filter(_id: 0, _name: NameFilterTextField.text, _player: 2);
        }else if(playerCount == 2){
            newFilter = Filter(_id: 0, _name: NameFilterTextField.text, _player: 3);
        }else {
            newFilter = Filter(_id: 0, _name: NameFilterTextField.text, _player: 0);
        }
        
        askServerForGameList(newFilter);

    }
    
    func addNewGamesToTable(list:Array<Filter>){
        gameNames = [];
        gameList = list;
        for value in list {
            gameNames.append(value.toString());
        }
        GameTableView.reloadData();
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
        selectedRowForSegue = indexPath.row;
        
        self.performSegueWithIdentifier("PlayGameViewSegue", sender: self);
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:CstmTableCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CstmTableCell;
        
        let row = indexPath.row;
        var fillArray = gameNames[row].characters.split {$0 == ","}.map { String($0) };
        cell.GameNameLabel.text = fillArray[0];
        cell.NmbOfPlayerLabel.text = fillArray[1];
        
        return cell;
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameNames.count;
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "PlayGameViewSegue"){
            let controller:PlayViewController = segue.destinationViewController as! PlayViewController;
            controller.socket = self.socket;
            controller.chosenGameFilter = self.gameList[selectedRowForSegue];
            
        }
    }


}
