//
//  Ranking.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 28.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation


class Ranking {
    
    var points:Int;
    var rank:Int;
    
    init(_points:Int, _rank:Int){
        points = _points;
        rank = _rank;
    }
    init(){
        points = 0;
        rank = 0;
    }

    
}