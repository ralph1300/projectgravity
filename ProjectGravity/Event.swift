//
//  Event.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 28.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation


class Event {
    
    var type: String;
    var content:AnyObject?;
    
    init(_type:String, _content:AnyObject?){
        type = _type;
        content = _content;
    }
    init(){
        type = "";
        content = nil;
    }
}