//
//  PairViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 26.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//


import UIKit


class PairViewController: UIViewController {
    
    

    @IBOutlet var PairView: UIView!
    @IBOutlet weak var LogoLabel: UILabel!
    var socket:SocketIOClient = SocketIOClient(socketURL: Statics.URL);
    var playerNumber:Int? = 0;
    @IBOutlet weak var PINTextField: UITextField!
    @IBOutlet weak var SendPINButton: UIButton!
    
    let animation = CABasicAnimation(keyPath: "position");
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        
        setCustomFondsAndColors();
        self.addHandlers();
        self.socket.connect();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func addHandlers(){
        self.socket.on("checkPin") {[weak self] data, ack in
            var dict = HelperMethods.parseJsonToDictionary(data?[0] as! String);
            
            self!.playerNumber = Int(dict["player"]!);
            
            //IF RESULT IS TRUE
            if(dict["status"] == "true"){
                if(self?.playerNumber == 0){
                    self!.performSegueWithIdentifier("GameViewController", sender: self);
                }else{
                    self!.performSegueWithIdentifier("PlayGameAsSecPlayerSegue", sender: self);
                }
                
            }else{
                self!.PINTextField.layer.addAnimation(self!.animation, forKey: "position");
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "GameViewController"){
            let controller:GameViewController = segue.destinationViewController as! GameViewController;
            controller.socket = self.socket;
        }
        if(segue.identifier == "PlayGameAsSecPlayerSegue"){
            let controller:PlayViewController = segue.destinationViewController as! PlayViewController;
            controller.socket = self.socket;
            controller.playerNumber = self.playerNumber!;
        }
        if(segue.identifier == "QRViewController"){
            let controller:QRViewController = segue.destinationViewController as! QRViewController;
            controller.socket = self.socket;
        }
    }
    
    @IBAction func QRCodeButtonClicked(sender: AnyObject) {
        self.performSegueWithIdentifier("QRViewController", sender: self);
    }

    
    @IBAction func PairDeviceButtonClicked(sender: AnyObject) {
        let pin:String = PINTextField.text;
        if(pin != ""){
            let dict = ["pin": NSString(string:pin), "status":false, "player": -1];
            let jsonString = HelperMethods.JSONStringify(dict).stringByReplacingOccurrencesOfString("\"", withString:   ";");
            self.socket.emit(Statics.CHECK_PIN, jsonString);
        }else{
            PINTextField.layer.addAnimation(animation, forKey: "position");
        }

        
    }
    
    
    func setCustomFondsAndColors(){
        LogoLabel.font = UIFont (name: "DK Crayon Crumble", size: 60);
        LogoLabel.textColor = UIColor(red:0.235 , green: 0.58, blue: 0.55, alpha: 1.0);
        PairView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        animation.duration = 0.07;
        animation.repeatCount = 4;
        animation.autoreverses = true;
        animation.fromValue = NSValue(CGPoint: CGPointMake(PINTextField.center.x - 10, PINTextField.center.y));
        animation.toValue = NSValue(CGPoint: CGPointMake(PINTextField.center.x + 10, PINTextField.center.y));
        //SendPINButton.frame = CGRectMake(SendPINButton.center.x, SendPINButton.center.y, 20, 20);
        //SendPINButton.layer.cornerRadius = 0.6 * SendPINButton.bounds.size.width;
        //SendPINButton.setImage(UIImage(named:"play2.png"), forState: .Normal);
        
    }
    
    
}
