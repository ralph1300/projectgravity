//
//  QRViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 02.06.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit
import AVFoundation

class QRViewController: UIViewController,  AVCaptureMetadataOutputObjectsDelegate {
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    @IBOutlet var QRView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var socket:SocketIOClient = SocketIOClient(socketURL: "");
    var playerNumber:Int? = 0;
    var alreadySend:Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        setCustomFondsAndColors();
        self.addHandlers();
        alreadySend = false;
        loadCamera();
        initializeQR();
        // Move the message label to the top view
        view.bringSubviewToFront(messageLabel);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func setCustomFondsAndColors(){
        QRView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        
    }
    func addHandlers(){
        self.socket.on("checkPin") {[weak self] data, ack in
            var dict = HelperMethods.parseJsonToDictionary(data?[0] as! String);
            
            self!.playerNumber = Int(dict["player"]!);
            
            //IF RESULT IS TRUE
            if(dict["status"] == "true"){
                if(self?.playerNumber == 0){
                    self!.performSegueWithIdentifier("FromQRToGameView", sender: self);
                }
                else{
                    self!.performSegueWithIdentifier("FromQRToPlay", sender: self);
                }
            }
            else{
                self!.performSegueWithIdentifier("WrongQRCode", sender: self);
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "FromQRToGameView"){
            let controller:GameViewController = segue.destinationViewController as! GameViewController;
            controller.socket = self.socket;
        }
        if(segue.identifier == "FromQRToPlay"){
            let controller:PlayViewController = segue.destinationViewController as! PlayViewController;
            controller.socket = self.socket;
            controller.playerNumber = self.playerNumber!;
        }
        if(segue.identifier == "WrongQRCode"){
            let controller:PairViewController = segue.destinationViewController as! PairViewController;
            controller.socket = self.socket;
        }

    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero;
            messageLabel.text = "No QR code is detected";
            return;
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject;
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject;
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                messageLabel.text = metadataObj.stringValue;
                if(!alreadySend){
                    trySendQRToServer(metadataObj.stringValue);
                    alreadySend = true;
                }
            }
        }
    }
    
    func trySendQRToServer(pin:String){
        let dict = ["pin": NSString(string:pin), "status":false, "player": -1];
        let jsonString = HelperMethods.JSONStringify(dict).stringByReplacingOccurrencesOfString("\"", withString:   ";");
        self.socket.emit(Statics.CHECK_PIN, jsonString);
    }
    
    func initializeQR(){
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView();
        qrCodeFrameView?.layer.borderColor = UIColor.greenColor().CGColor;
        qrCodeFrameView?.layer.borderWidth = 2;
        view.addSubview(qrCodeFrameView!);
        view.bringSubviewToFront(qrCodeFrameView!);
        
        
    }
    
    func loadCamera(){
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo);
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        var error:NSError?;
        let input: AnyObject!
        do {
            input = try AVCaptureDeviceInput.deviceInputWithDevice(captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
        };
        
        if (error != nil) {
            // If any error occurs, simply log the description of it and don't continue any more.
            print("\(error?.localizedDescription)");
            return
        }
        
        // Initialize the captureSession object.
        captureSession = AVCaptureSession()
        // Set the input device on the capture session.
        captureSession?.addInput(input as! AVCaptureInput);
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let captureMetadataOutput = AVCaptureMetadataOutput();
        captureSession?.addOutput(captureMetadataOutput);
        
        // Set delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue());
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode];
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill;
        videoPreviewLayer?.frame = view.layer.bounds;
        view.layer.addSublayer(videoPreviewLayer);
        
        // Start video capture.
        captureSession?.startRunning();

        
        
    }
    
    
}

