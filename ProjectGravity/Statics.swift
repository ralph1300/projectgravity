//
//  Statics.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 12.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

public class Statics{
    
    static var URL:String = "http://projectgravity-sfraungruber.rhcloud.com:8000";
    //static var URL:String = "localhost:3000";
    static var fondSize:Float = 56;
/*
The following strings define the events
*/
    static var POSITION_UPDATE:String = "positionUpdate";
    static var EVENT:String = "event";
    static var START_GAME:String = "startGame";
    static var SHOOT:String = "key";

/*
The following field defines the time, when the json should be send to the server in milliseconds
*/
    static var SEND_NEW_JSON_TIMER:Int = 2;

/*
The following strings define the json fields
*/
    static var CHECKED_PIN:String = "checkedPin";
    static var CHECK_PIN:String = "checkPin";
    static var LOAD_GAMES:String = "loadGameList";
    static var SELECT_GAME:String = "selectGame";
    
    static var pin:String = "pin";

    static var X_AXIS:String = "xAxis";
    static var Y_AXIS:String = "yAxis";
    static var Z_AXIS:String = "zAxis";
}