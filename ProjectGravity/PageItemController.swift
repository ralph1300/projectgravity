//
//  PageItemController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 24.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit


class PageItemController: UIViewController {

    

    @IBOutlet var PageContentView: UIView!
    
    @IBOutlet weak var ImageView: UIImageView!
    
    var pageIndex:Int = 0;
    var imageName: String = "" {
        
        didSet {
            
            if let imageView = ImageView {
                imageView.image = UIImage(named: imageName);
            }
            
        }
    };
    
    override func viewDidLoad() {
        super.viewDidLoad();
        setCustomFondsAndColors();
        ImageView!.image = UIImage(named: imageName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func setCustomFondsAndColors(){
        PageContentView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        
    }


}
