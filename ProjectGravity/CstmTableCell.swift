//
//  CstmTableCell.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 25.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit

class CstmTableCell: UITableViewCell {

    @IBOutlet weak var GameNameLabel: UILabel!
    @IBOutlet weak var NmbOfPlayerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
