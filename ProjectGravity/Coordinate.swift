//
//  Coordinate.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 26.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

class Coordinate{
    
    var x: Double;
    var y: Double;
    var z: Double;
    
    init(_x:Double, _y:Double, _z:Double){
        x = _x;
        y = _y;
        z = _z;
    }
    init(){
        x = 1;
        y = 1;
        z = 1;
    }
    
    func coordinateToDictionary() -> [String: NSObject] {
        return [
            "x": x,
            "y": y,
            "z": z
        ]
    }
    func degreesToRadians(){
        x =  (180 * x / M_PI) * (-1);
        y =  (180 * y / M_PI) * (-1);
        z =  (180 * z / M_PI) * (-1);
    }

    
}