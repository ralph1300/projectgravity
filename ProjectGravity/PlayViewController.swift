//
//  PlayViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 25.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit
import CoreMotion


class PlayViewController: UIViewController {
    
    
    @IBOutlet weak var LogoLabel: UILabel!
    @IBOutlet var PlayView: UIView!
    @IBOutlet weak var StartButton: UIButton!
    @IBOutlet weak var ExplLabel: UILabel!
    @IBOutlet weak var ShootButton: UIButton!
    @IBOutlet weak var ExplLabel2: UILabel!
    
    var socket:SocketIOClient = SocketIOClient(socketURL: "");
    var chosenGameFilter:Filter = Filter();
    var playerNumber:Int = 0;
    let motionManager = CMMotionManager();
    var contentDict:NSDictionary = NSDictionary();
    var loadingView: UIView = UIView();
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView();
    var container: UIView = UIView();
    

    override func viewDidLoad() {
        super.viewDidLoad();
        self.addHandlers();
        sendSelectedGame();
        loadUIForNeededPlayer();
        
        setCustomFondsAndColors();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func addHandlers(){
        //Adds Handlers for the Socket, so this View can work with data from the server
        //{;type;:;gameReady;,;content;:{;player;:1,;name;:;Space Invaders;,;id;:2}}
        self.socket.on(Statics.EVENT) {[weak self] data, ack in
            
            let jsonString = (data?[0] as! String).stringByReplacingOccurrencesOfString(";", withString: "\"");
            let JSONdata = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!;
            var error:NSError?;
            let dict: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(JSONdata, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary;
            self!.contentDict = dict["content"] as! NSDictionary;
            
            if(dict["type"] as! String == "gameReady"){
                self!.setNeededUIChanges();
                self!.startCapturingGyroDataForGame();
                self!.playerNumber = self!.contentDict["player"] as! Int;
            
            }
            if(dict["type"] as! String == "gameOver"){
                self!.StartButton.setTitle("Start the Game!", forState: UIControlState.Normal);
                self!.motionManager.stopDeviceMotionUpdates();
                self!.motionManager.stopAccelerometerUpdates();
                self!.performSegueWithIdentifier("GameOverViewController", sender: self);
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "GameOverViewController"){
            let controller:GameOverViewController = segue.destinationViewController as! GameOverViewController;
            controller.socket = self.socket;
            controller.ranking = Ranking(_points: self.contentDict["points"] as! Int,_rank: contentDict["ranking"] as! Int);
            controller.chosenGameFilter = self.chosenGameFilter;
        }
    }
    
    
    
    func startCapturingGyroDataForGame(){
        if motionManager.accelerometerAvailable {
            motionManager.deviceMotionUpdateInterval = 0.02;
            motionManager.startDeviceMotionUpdates();
            
            motionManager.accelerometerUpdateInterval = 0.02;
            motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue()) {
                [weak self] (data: CMAccelerometerData!, error: NSError!) in
                
                if (fabs(data.acceleration.x) > 0.2) {
                    self?.sendRotationDataToServer(data.acceleration);
                    if (error != nil) {
                        print("\(error)");
                    }
                }
                
            }
        }
        else {
            print("ERROR");
        }
    }
    
    func sendRotationDataToServer(rotation: CMAcceleration){
        let cord:Coordinate = Coordinate(_x: rotation.x, _y: rotation.y, _z: rotation.z);
        cord.degreesToRadians();
        let jsonString = HelperMethods.getJSONForEvent(Statics.POSITION_UPDATE,content:cord);
        
        self.socket.emit(Statics.EVENT,jsonString.stringByReplacingOccurrencesOfString("\"", withString: ";"));
    }
    

    
    func sendSelectedGame(){
        let jsonString = HelperMethods.getJSONForEvent(Statics.SELECT_GAME, content: chosenGameFilter).stringByReplacingOccurrencesOfString("\"", withString: ";");
        
        self.socket.emit(Statics.EVENT,jsonString);
    }

    @IBAction func startTheGameButtonClicked(sender: AnyObject) {
        let jsonString = HelperMethods.getJSONForEvent(Statics.START_GAME, content: nil).stringByReplacingOccurrencesOfString("\"", withString: ";");
        if(self.chosenGameFilter.player == 1){
            self.ShootButton.enabled = true;
            self.ShootButton.hidden = false;
        }
        StartButton.enabled = false;
        StartButton.hidden = true;
        self.socket.emit(Statics.EVENT,jsonString);
    }

    @IBAction func shootButtonClicked(sender: AnyObject) {
        let jsonString = HelperMethods.getJSONForEvent(Statics.SHOOT, content: nil).stringByReplacingOccurrencesOfString("\"", withString: ";");
        
        self.socket.emit(Statics.EVENT,jsonString);
    }
    
    func loadUIForNeededPlayer(){
        
        ShootButton.hidden = true;
        ShootButton.enabled = false;
        ExplLabel.hidden = true;
        ExplLabel2.hidden = true;
        
        if(playerNumber != 0){
            showActivityIndicatory(self.view, text: "");
            StartButton.hidden = true;
            StartButton.enabled = false;
            ExplLabel2.hidden = true;
            
        }else{
            showActivityIndicatory(self.view, text: "");
            StartButton.hidden = false;
            StartButton.enabled = false;
        }
        
    }
    
    func setNeededUIChanges(){
        self.ExplLabel.hidden = false;
        hideActivityIndicator(self.view);
        if(self.playerNumber == 0){
            self.StartButton.enabled = true;
            self.ExplLabel2.hidden = false;
        }
    }
    
    func setCustomFondsAndColors(){
        LogoLabel.font = UIFont (name: "DK Crayon Crumble", size: 60);
        LogoLabel.textColor = UIColor(red:0.235 , green: 0.58, blue: 0.55, alpha: 1.0);
        PlayView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        
    }
    func showActivityIndicatory(uiView: UIView, text:String) {
        container = UIView();
        container.frame = uiView.frame;
        container.center = uiView.center;
        container.backgroundColor = UIColor(netHex:0xffffff, al: 0.3);
        
        loadingView = UIView();
        loadingView.frame = CGRectMake(0, 0, 80, 80);
        loadingView.center = uiView.center;
        loadingView.backgroundColor = UIColor(netHex:0x444444, al: 0.7);
        loadingView.clipsToBounds = true;
        loadingView.layer.cornerRadius = 10;
        
        actInd = UIActivityIndicatorView();
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge;
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
            loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd);

        if(text != ""){
            let label: UILabel = UILabel();
            label.text = text;
            label.frame = CGRectMake(0, 0, 40, 20);
            label.center = CGPointMake(loadingView.frame.size.width / 2 + 25,
                loadingView.frame.size.height / 2);
            loadingView.addSubview(label);
            
        }
        container.addSubview(loadingView);
        uiView.addSubview(container);
        actInd.startAnimating();
    }
    
    func hideActivityIndicator(uiView: UIView) {
        actInd.stopAnimating()
        container.removeFromSuperview()
    }
    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, al:CGFloat) {
        assert(red >= 0 && red <= 255, "Invalid red component");
        assert(green >= 0 && green <= 255, "Invalid green component");
        assert(blue >= 0 && blue <= 255, "Invalid blue component");
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: al);
    }
    
    convenience init(netHex:Int, al:CGFloat) {
        let r = ((netHex >> 16) & 0xff);
        let g = (netHex >> 8) & 0xff;
        let b = netHex & 0xff;
        self.init(red:CGFloat(r), green:CGFloat(g), blue:CGFloat(b), alpha: al);
    }
}
