//
//  HelperMethods.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 21.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

class HelperMethods{
    
    static func JSONStringify(value: AnyObject) -> String {
        //Turns an Object into a JSON String
        if NSJSONSerialization.isValidJSONObject(value) {
            if let data = try? NSJSONSerialization.dataWithJSONObject(value, options: []) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string as String;
                }
            }
        }
        return "";
    }
    
    static func parseJsonToDictionary(jsonString: String) -> Dictionary<String,String>{
        //Parses an String to a better accessable Dictionary, expected format:
        //"{;status;:false,;player;:-1,;pin;:;1234;}"
        var dict:Dictionary = [String:String]();
        let bufferString = jsonString.stringByReplacingOccurrencesOfString(",", withString: "").stringByReplacingOccurrencesOfString(":", withString: "").stringByReplacingOccurrencesOfString("{", withString: "").stringByReplacingOccurrencesOfString("}", withString: "");
        
        var jsonArray = bufferString.characters.split {$0 == ";"}.map { String($0) };
        
        if(jsonArray.count % 2 == 0){
            var indexSaver:Int = 1;
            for (index, value) in jsonArray.enumerate() {
                if(index == indexSaver-1){
                        dict[value] = jsonArray[indexSaver];
                }
                else{
                    indexSaver += 2;
                }
            }
        }else{
            print("Something is wrong with the jsonString -> No KEY:VALUE");
        }
        
        return dict;
    }

    static func getJSONForEvent(type:String, content: AnyObject?) -> String{
        //var dict:Dictionary = [String:Dictionary<String, NSObject>]();
        var dict:Dictionary = [String:AnyObject]();
        switch type{
            case "selectGame":
                let filter = (content as! Filter).filterToDictionary();
                dict["type"] = type;
                dict["content"] = filter;
                break;
            case "positionUpdate":
                let coordi = (content as! Coordinate).coordinateToDictionary();
                dict["type"] = type;
                dict["content"] = coordi;
                break;
            case "key":
                dict["type"] = type;
                dict["content"] = "B";
                break;
            default:
                dict["type"] = type;
                dict["content"] = nil;
                break;
        }
        return self.JSONStringify(dict);
    }
    
    static func parseJsonToFilterArray(anyObj:AnyObject) -> Array<Filter>{
        //Parses a JSON to an Array
        
        var list:Array<Filter> = [];
        
        if  anyObj is Array<AnyObject> {

            for json in anyObj as! Array<AnyObject>{
                let b:Filter = Filter();
                b.name = (json["name"] as AnyObject? as? String) ?? "";
                b.id  =  (json["id"]  as AnyObject? as? Int) ?? 0;
                b.player  =  (json["player"]  as AnyObject? as? Int) ?? 0;
                
                list.append(b);
            }
        }
        return list;
    }

}