//
//  Filter.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 21.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

class Filter{

    var id: Int;
    var name: String;
    var player: Int;
    
    init(_id:Int, _name:String, _player:Int){
        id = _id;
        name = _name;
        player = _player;
    }
    init(){
        id = 0;
        name = "";
        player = 1;
    }
    
    func toString() -> String{
        let str:String = name + "," + "Players: " + String(player);
        return str;
    }
    
    func filterToDictionary() -> [String: NSObject] {
        return [
            "id": id,
            "name": name,
            "player": player
        ];
    }

}