//
//  GameOverViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 28.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit


class GameOverViewController: UIViewController {
    
    
    @IBOutlet weak var LogoLabel: UILabel!
    @IBOutlet var GameOverView: UIView!
    var socket:SocketIOClient = SocketIOClient(socketURL: "");
    var ranking:Ranking = Ranking();
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    var chosenGameFilter:Filter = Filter();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        setCustomFondsAndColors();
        changeLabelsToRank();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func setCustomFondsAndColors(){
        GameOverView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);
        LogoLabel.font = UIFont (name: "DK Crayon Crumble", size: 60);
        LogoLabel.textColor = UIColor(red:0.235 , green: 0.58, blue: 0.55, alpha: 1.0);

    }
    func changeLabelsToRank(){
        pointLabel.text = String(ranking.points);
        rankLabel.text = String(ranking.rank);
    }
    
    @IBAction func startAgainButtonClicked(sender: AnyObject) {
        //StartAgainSegue
        self.performSegueWithIdentifier("StartAgainSegue", sender: self);

    }
    
    @IBAction func newGameButtonClicked(sender: AnyObject) {
        //SelectNewGameSegue
        self.performSegueWithIdentifier("SelectNewGameSegue", sender: self);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "SelectNewGameSegue"){
            let controller:GameViewController = segue.destinationViewController as! GameViewController;
            controller.socket = self.socket;
        }
        if(segue.identifier == "StartAgainSegue"){
            let controller:PlayViewController = segue.destinationViewController as! PlayViewController;
            controller.socket = self.socket;
            controller.chosenGameFilter = self.chosenGameFilter;
        }
    }
}
