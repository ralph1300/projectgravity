//
//  ViewController.swift
//  ProjectGravity
//
//  Created by Ralph Schnalzenberger on 11.05.15.
//  Copyright (c) 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{

    @IBOutlet var StartView: UIView!;
    let picturesForPageView = ["Page1.png","Page2.png","Page3.png","Page4.png"];
    var pageViewController:UIPageViewController?;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        createPageViewController();
        setupPageControl();
        self.setCustomFondsAndColors();
        
        
    }
    func setCustomFondsAndColors(){
        StartView.backgroundColor = UIColor(red: 0.933, green: 0.74, blue: 0.38, alpha: 1.0);

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    private func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController;
        pageController.dataSource = self;
        
        if picturesForPageView.count > 0 {
            let firstController = getItemController(0)!;
            let startingViewControllers: NSArray = [firstController];
            pageController.setViewControllers(startingViewControllers as [AnyObject] as [AnyObject], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil);
        }
        
        pageViewController = pageController;
        pageViewController?.view.frame = CGRect(x: 0, y: 30, width: self.view.frame.size.width, height: self.view.frame.size.height - 110);
        self.addChildViewController(pageViewController!);
        self.view.addSubview(pageViewController!.view);
        pageViewController!.didMoveToParentViewController(self);
        
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance();
        appearance.pageIndicatorTintColor = UIColor.grayColor();
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor();
        
    }
    
    
   func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
    let itemController = viewController as! PageItemController;
    
        if itemController.pageIndex > 0 {
            return getItemController(itemController.pageIndex-1);
        }
        
    return nil;
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController;
        
        if itemController.pageIndex+1 < picturesForPageView.count {
            return getItemController(itemController.pageIndex+1);
            
        }
        
        return nil;
    }
    
    private func getItemController(itemIndex: Int) -> PageItemController? {
        
        if itemIndex < picturesForPageView.count {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("PageContentViewController") as! PageItemController;
            pageItemController.pageIndex = itemIndex;
            pageItemController.imageName = picturesForPageView[itemIndex];
            return pageItemController;
        }
        
        return nil;
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return picturesForPageView.count;
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    

    
}

